[![PyPI pyversions](https://img.shields.io/pypi/pyversions/sardana.svg)](https://pypi.python.org/pypi/sardana/)
[![PyPI license](https://img.shields.io/pypi/l/sardana.svg)](https://pypi.python.org/pypi/sardana/)
[![PyPI version](https://img.shields.io/pypi/v/sardana.svg)](https://pypi.python.org/pypi/sardana/)
[![GitLab CI status](https://gitlab.com/sardana-org/sardana/badges/develop/pipeline.svg)](https://gitlab.com/sardana-org/sardana/-/commits/develop)  
[![Appveyor status](https://ci.appveyor.com/api/projects/status/rxeo3hsycilnyn9k/branch/develop?svg=true)](https://ci.appveyor.com/project/taurusorg/sardana/branch/develop)


Sardana is a software suite for Supervision, Control and Data Acquisition in scientific installations.

Projects related to Sardana
===========================

* Sardana uses Taurus for control system access and user interfaces
* Sardana is based on Tango
* The command line interface for Sardana (Spock) is based on IPython

Main web site: http://sardana-controls.org
